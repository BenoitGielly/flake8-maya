# flake8-maya

A simple python module to check for Autodesk Maya's short name flags used in your code.

Simply install this extension using pip:

    pip install flake8-maya-flags

Pip homepage: https://pypi.org/project/flake8-maya-flags/
